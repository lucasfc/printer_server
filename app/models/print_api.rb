class PrintApi
  def self.provider
    "http://192.168.40.40"
  end

  def self.printer
    "/d:USB3"
  end

  def self.print_url(entry)
    "#{provider}/entries/#{entry}/etiqueta"
  end

  def self.save_html(entry)
    response = HTTParty.get(self.print_url(entry))
    File.open("#{Rails.root}/public/entradas/#{entry}.html", 'w') {|f| f.write(response.body) }
  end

  def run_print(entry)
    save_html(entry)
    system "print #{printer} #{Rails.root}/public/entradas/#{entry}.html"
  end

end