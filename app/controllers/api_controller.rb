class ApiController < ApplicationController
  def print
    begin
      PrintApi.run_print(params[:entry])
      render json: {status: true}
    rescue
      render json: {status: false}
    end
  end
end
